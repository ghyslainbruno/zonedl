﻿#!/bin/sh

old_IFS=$IFS  				
IFS=$'\n' 

# echo bla > trytry.txt
rm data/*.dat

clear
# rm movies/search/*.dat
# rm *.html
# rm *.txt

string_to_search=`cat temp/file_title_search.txt`
# string_to_search=$1
# string_to_search="oblivion"
# category_to_search='&catlist%5B%5D=1&orderby=popular'
category_to_search='&displaychangeto=large'
string_to_search_formated=`echo $string_to_search | tr "[A-Z]" "[a-z]" | sed -e 's/\ /+/g'`

curl -s "http://www.zone-telechargement.com/films-gratuit.html?q=$string_to_search_formated$category_to_search" > temp/page_search.html
name='temp/page_search.html'

# nbr_different_titles=`cat temp/titles.txt | sort | uniq | wc -l`
# tab=()
# for i in `seq 1 $nbr_different_titles`
# do
	# tab[$i]='0'
	# echo $i
# done

# nbr_different_titles=`cat temp/titles.txt | sort | uniq | wc -l`
# for i in `seq 1 $nbr_different_titles`
# do
   # eval tab$i="0"
   # # eval echo "\$tab$i"
# done


# echo 'Récupération des titres...'
# cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep '<b><a href=\"' | sed -e 's/^.*<b><a href=\".*\" >//g' | sed -e 's/<\/a><\/b>//g' > temp/titles.txt
cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep '<a href=".*$' | sed -e 's/^.*www\.zone-telechargement\.com\/[a0-z9].*\">//g' | sed -e 's/<.*$//g' > temp/titles.txt
	
nbr_different_titles=`cat temp/titles.txt | sort | uniq | wc -l`
for i in `seq 1 $nbr_different_titles`
do
   eval tab$i="0"
   # # # eval echo "\$tab$i"
done

	
	
	# echo 'Extraction des données...'
	nbr_file_same_titles=0
	nbr_file=0
	for title in `cat temp/titles.txt | grep -Ev '([Ii][Nn][Tt][Eeé]*[Gg][Rr][Aa][Ll][Ee]|[Cc][Oo][Mm][Pp][Ll][Eeè]*[Tt][Ee]*|[Aa][Ss][Ss][Ee][Mm][Bb][Ll][Ee][Dd])'`
	do
		index_for_tab=`cat temp/titles.txt | sort | uniq | grep -n '^.*$' | grep ":$title$" | grep -o '^[0-9]'`
		nbr_same_titles=`cat $name | sed -n '/<div\ class=\"maincont\">/,/Suite et Télécharger.../p' | grep "<a href=\"http://www.zone-telechargement.com/[a0-z9].*\">$title<\/a><\/h1>" | grep -n 'a\ href' | wc -l`
		nbr_file=$(($nbr_file+1))
		title_dat=`echo ${title}_${nbr_file}.dat | tr "[A-Z]" "[a-z]" | tr " " "_" | sed -e 's/é/e/g' | sed -e 's/è/e/g' | sed -e 's/\.//g' | sed -e 's/://g' | sed -e 's/-//g' | sed -e "s/'/_/g" | sed -e 's/dat/\.dat/g' | sed -e 's/\*//g'`
		
		# echo $value_tab
		if [ "$nbr_same_titles" -gt "1" ]
		then
			eval value_tab=\$tab$index_for_tab
			# nbr_file_same_titles=`expr $nbr_file_same_titles + 1`
			nbr_file_same_titles=`echo $(($value_tab+1))`
			# tab[$index_for_tab]=$nbr_file_same_titles
			eval tab$index_for_tab=$(($nbr_file_same_titles))
		fi
		
		# echo $title_dat
		# echo $nbr_same_titles
		# echo $nbr_file
		title_to_kodi=`echo $title | sed -e 's/*and\*/\&/g' | sed -e 's/-and-/\&/g' | sed -e "s/_/'/g"`
		
		title_temp=`echo $title | sed -e 's/"/\&amp;#039;/g' | sed -e "s/'/\&#039;/g" | sed -e 's/_and_/_\&amp;_/g' | sed -e "s/_/\&amp;amp;#039;/g" | sed -e 's/*and*/\&amp;/g' | sed -e 's/-and-/\&amp;amp;amp;/g'`
		
		# echo $nbr_file
		# echo $nbr_same_titles
		# echo $nbr_file_same_titles
		
		echo $title_to_kodi > data/$title_dat
		echo $title_to_kodi
		
		### Lien de la page ###
		if [ "$nbr_same_titles" -gt "1" ]
		then
			link_zonedl=`cat $name | sed -n '/<div\ class=\"maincont\">/,/Suite et Télécharger.../p' | grep "<a href=\"http://www.zone-telechargement.com/[a0-z9].*\">$title" | grep -n 'a\ href' | grep "$nbr_file_same_titles:" | grep -o 'http://www.zone-telechargement.com/[a0-z9].*\"' | sed -e 's/\"//g'`
			# echo $link_zonedl
		fi
		if [ "$nbr_same_titles" = "1" ]
		then
			link_zonedl=`cat $name | sed -n '/<div\ class=\"maincont\">/,/Suite et Télécharger.../p' | grep "<a href=\"http://www.zone-telechargement.com/[a0-z9].*\">$title</a></h1>" | grep -o 'http://www.zone-telechargement.com/[a0-z9].*\"' | sed -e 's/\"//g'`
			# echo $link_zonedl
		fi
		echo $link_zonedl >> data/$title_dat
		echo $link_zonedl
		#######################
		
		### Lien de l'icône ### 
		if [ "$nbr_same_titles" -gt "1" ]
		then
			# icon_link=`cat $name | sed -n "/>$title<\/a><\/b>/,/Suite et Télécharger.../p" | grep -Eo "http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)" | sed -e 's/alt/\n/g' | grep '^http' | grep -n '^.*$' | grep "$nbr_file_same_titles:" | grep -Eo "http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg)"`
			## Test pour résoudre pblm ## 
			icon_link=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -Eo 'http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)' | sed -e 's/alt/\n/g' | grep '^http' | grep -n '^.*$' | grep "$nbr_file_same_titles:" | sed -e 's/<!--dle_image_end-->/\n/g' | grep -Eo 'http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)'`
			# echo $icon_link
		fi
		if [ "$nbr_same_titles" = "1" ]
		then
			# icon_link=`cat $name | sed -n "/>$title<\/a><\/b>/,/Suite et Télécharger.../p" | grep -Eo "http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)" | sed -e 's/alt/\n/g' | grep -Eo "http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg)"`
			## Test pour résoudre pblm ## 
			icon_link=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -Eo 'http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)' | sed -e 's/alt/\n/g' | sed -e 's/<!--dle_image_end-->/\n/g' | grep -Eo 'http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg|gif)'`
			# echo $icon_link
		fi
		echo $icon_link
		echo $icon_link >> data/$title_dat
		#######################

		
		
		
		
		######Pour la note #######
		# cat $name | sed -n '/<div\ class=\"maincont\">/,/Suite et Télécharger.../p' | grep "$title" | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -no '[0-9]*[0-9]*,*\.*[0-9]/5' | grep "$nbr_file:" | grep -o '[0-9]*[0-9]*,*[0-9]/5'
		if [ "$nbr_same_titles" -gt "1" ]
		then
			note_bool=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -no '[0-9]*[0-9]*,*\.*[0-9]/5' | grep "$nbr_file_same_titles:" | grep -o '[0-9]*[0-9]*,*[0-9]/5' | wc -l`
			if [ "$note_bool" = "0" ] ;
			then 
				note_bool="0"
				echo -e '0' >> data/$title_dat
			else
				###Pour avoir la note, mais /5 juste ici
				note_sur_5=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -no '[0-9]*[0-9]*,*\.*[0-9]/5' | grep "$nbr_file_same_titles:" | grep -o '[0-9]*[0-9]*,*[0-9]/5' | sed -e 's/\/5//g' | sed -e 's/,/\./g'`
				###On a bien la note sur 10 ici
				note_sur_10=`awk -vp=$note_sur_5 'BEGIN{printf "%.1f\n" ,p * 2}'`
				echo $note_sur_10 >> data/$title_dat
				echo $note_sur_10
			fi
		fi

		
		
		
		# cat $name | sed -n '/<div\ class=\"maincont\">/,/Suite et Télécharger.../p' | grep "$title" | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -o '[0-9]*[0-9]*,*\.*[0-9]/5'
		if [ "$nbr_same_titles" = "1" ]
		then
			note_bool=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -o '[0-9]*[0-9]*,*\.*[0-9]/5' | wc -l`
			if [ "$note_bool" = "0" ] ;
			then 
				note_bool="0"
				echo -e '0' >> data/$title_dat
			else
				###Pour avoir la note, mais /5 juste ici
				note_sur_5=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Note spectateurs.*[0-9]*[0-9]*,*\.*[0-9]/5' | grep -o '[0-9]*[0-9]*,*\.*[0-9]/5' | sed -e 's/\/5//g' | sed -e 's/,/\./g'`
				###On a bien la note sur 10 ici
				note_sur_10=`awk -vp=$note_sur_5 'BEGIN{printf "%.1f\n" ,p * 2}'`
				echo $note_sur_10 >> data/$title_dat
				echo $note_sur_10
			fi
		fi
		
		
		echo No quality needed >> data/$title_dat

		
		###Durée en heure ici
		# duration_hours=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep "$title_temp" | grep -o '[0-9]h\ *[0-9][0-9]*min' | sed -e 's/min//g' | sed -e 's/h/\./g' | sed -e 's/\ //g'`
		if [ "$nbr_same_titles" -gt "1" ]
		then
			duration_hours=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '[0-9]h\ *[0-9][0-9]*min' | grep -n 'min' | grep "$nbr_file_same_titles:" | grep -o '[0-9]h\ *[0-9][0-9]*min' | sed -e 's/min//g' | sed -e 's/h/\./g' | sed -e 's/\ //g'`
		fi		
		
		
		if [ "$nbr_same_titles" = "1" ]
		then
			duration_hours=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '[0-9]h\ *[0-9][0-9]*min' | sed -e 's/min//g' | sed -e 's/h/\./g' | sed -e 's/\ //g'`
		fi
		
		duration_seconds=`awk -vp=$duration_hours 'BEGIN{printf "%.1f\n" ,p * 3600}'`
		echo $duration_seconds
		echo $duration_seconds >> data/$title_dat
		
		
		##### Genre ######
		if [ "$nbr_same_titles" -gt "1" ]
		then
			genre=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Genre.*Durée' | grep -n 'Genre' |sed -e 's/Genre\ \: <\/b>//g' | sed -e 's/<br\ \/><b>Durée//g' | grep "$nbr_file_same_titles" | sed -e 's/[0-9]://g' | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g"| sed -e 's/<\/font><\/span>//g'`
		fi
		
		if [ "$nbr_same_titles" = "1" ]
		then
			genre=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o 'Genre.*Durée' | grep -n 'Genre' |sed -e 's/Genre\ \: <\/b>//g' | sed -e 's/<br\ \/><b>Durée//g' | sed -e 's/[0-9]://g' | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g"| sed -e 's/<\/font><\/span>//g'`
		fi

		echo $genre
		echo $genre >> data/$title_dat
		##################
		
		
		
		##### Réalisateur #####
		if [ "$nbr_same_titles" -gt "1" ]
		then
			if [ `cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep 'Acteurs' | wc -l` = '0' ]
			then
				director=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '<b>Réalisateur : </b>.*<br /><b>Genre' | sed -e 's/<b>Réalisateur\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Genre//g' | grep -n '^.*$' | grep "$nbr_file_same_titles" | sed -e 's/[0-9]://g' | sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g'`
			else
				director=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '<b>Réalisateur : </b>.*<br /><b>Acteurs' | sed -e 's/<b>Réalisateur\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Acteurs//g' | grep -n '^.*$' | grep "$nbr_file_same_titles" | sed -e 's/[0-9]://g' | sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g'`
			fi
		fi
		
		if [ "$nbr_same_titles" = "1" ]
		then
			if [ `cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep 'Acteurs' | wc -l` = '0' ]
			then
				director=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '<b>Réalisateur : </b>.*<br /><b>Genre' | sed -e 's/<b>Réalisateur\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Genre//g' | sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g'`
			else
				director=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | grep -o '<b>Réalisateur : </b>.*<br /><b>Acteurs' | sed -e 's/<b>Réalisateur\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Acteurs//g' | sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g'`
			fi
		fi
		echo $director
		echo $director >> data/$title_dat
		#######################
		
		
		
		
		### Année ###
		if [ "$nbr_same_titles" -gt "1" ]
		then
			bool_year=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]' | grep -n '^.*$' | grep "$nbr_file_same_titles:" | grep -o '[0-9][0-9][0-9][0-9]' | wc -l`
			year=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]' | grep -n '^.*$' | grep "$nbr_file_same_titles:" | grep -o '[0-9][0-9][0-9][0-9]'`
			year_prod=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o 'Année de production : </b>[0-9][0-9][0-9][0-9]' | sed -e 's/Année\ de\ production\ :\ <\/b>\([0-9][0-9][0-9][0-9]\)/\1/g' | grep -n '^.*$' | grep "$nbr_file_same_titles:" | grep -o '[0-9][0-9][0-9][0-9]'`
			if [ "$bool_year" = "0" ] ;
			then
				echo $year_prod >> data/$title_dat
				echo $year_prod
			else
				echo $year >> data/$title_dat
				echo $year
			fi
		fi
		
		if [ "$nbr_same_titles" = "1" ]
		then
			bool_year=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]' | wc -l`
			year=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]'`
			year_prod=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g" | sed -e "s/<\/font><\/span>//g" | grep -o 'Année de production : </b>[0-9][0-9][0-9][0-9]' | sed -e 's/Année\ de\ production\ :\ <\/b>\([0-9][0-9][0-9][0-9]\)/\1/g'`
			if [ "$bool_year" = "0" ] ;
			then
				echo $year_prod >> data/$title_dat
				echo $year_prod
			else
				echo $year >> data/$title_dat
				echo $year
			fi
		fi
		#############
		
		### Synopsis ### 
		synopsis=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/Do\ Not\ Click\ :)/p' | tr -d "\t" | sed '/^$/d' | sed -e 's/^.*Do\ Not\ Click\ :).*$/\n/g' | sed '/^<\/div>$/d' | sed '/^<div\ class=".*">$/d' | sed ':a;N;$!ba;s/<\/a><\/h1>\n/ /g'  | grep $title | sed -e 's/^.*synopsis.png//g' | sed -e 's/<div\ align=\"center\">/\ndiv_align_center/g' | grep 'div_align_center' | grep -v 'release' | sed -e 's/div_align_center//g' | sed -e "s/<span\ style='background-color:yellow;'><font\ color='red'>//g"| sed -e 's/<\/font><\/span>//g' | grep -v 'release' | sed -e ''s/\<.*\>//g | sed -e "s/&#039;/'/g" | sort | uniq`
		# synopsis=`cat $name | sed -n "/>$title<\/a><\/b>/,/Suite et Télécharger.../p" | sed -e 's/^.*synopsis.png//g' | sed -e 's/<div\ align=\"center\">/\ndiv_align_center/g' | grep 'div_align_center' | grep -v 'release' | sed -e 's/div_align_center//g' | sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g' | sed -e ''s/\<.*\>//g | uniq`
		echo $synopsis >> data/$title_dat
		echo $synopsis
		################
		
		echo -e "\n"
done


# cat $name | sed -n "/>$title<\/a><\/b>/,/Suite et Télécharger.../p" | sed -e 's/^.*synopsis.png//g' | sed -e 's/<div\ align=\"center\">/\ndiv_align_center/g' | grep 'div_align_center' | grep -v 'release' | sed -e 's/div_align_center//g' | sed -e ''s/\<.*\>//g | uniq


# sed -e "s/<span\ style=\'background-color\:yellow\;\'><font\ color=\'red\'>//g" | sed -e 's/<\/font><\/span>//g'
          # <span\ style=\'background-color\:yellow\;\'><font color=\'red\'>
# nbr_different_titles=`cat temp/titles.txt | sort | uniq | wc -l`
# tab=()
# for i in `seq 1 $nbr_different_titles`
# do
	# tab[$i]='0'
# done

# nbr_different_titles=`cat temp/titles.txt | sort | uniq | wc -l`
# for i in `seq 1 $nbr_different_titles`
# do
   # eval tab$i="0"
   # eval echo "\$tab$i"
# done


# echo ${tab[*]}

# &#039; --> appostrophe
# Faire en sorte qu'il regarde que les films (hd etc..) mais pas séries ou autres... --> fait avec la category_to_search
# Faire en sorte qu'il gère si il a plusieurs titres en plusieurs fois... ne marche que pour 1 pour le moment...