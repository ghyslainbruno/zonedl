﻿# import os
# import sys
# import xbmcgui
# import xbmcplugin
# import time

# os.chdir("/storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films/search")
# addon_handle = int(sys.argv[1])
# xbmcplugin.setContent(addon_handle, 'movies')

# dialog = xbmcgui.Dialog()
# launch_bool = dialog.yesno("Recherche", "Voulez-vous rechercher un film ?", yeslabel='Oui', nolabel='Non')

# if launch_bool == 0:
	# sys.exit(0)


# dialog_search = xbmcgui.Dialog()
# search_name = dialog_search.input('Quel film voulez-vous rechercher ?', type=xbmcgui.INPUT_ALPHANUM)
# print search_name



################## Affichage des résultats ####################
# for fich in os.listdir('.'):
	# fichier = open(fich, "r")		
	# title=fichier.readline().rstrip('\n\r')
	# url_zonedl=fichier.readline().rstrip('\n\r')
	# icon=fichier.readline().rstrip('\n\r')
	# note=fichier.readline().rstrip('\n\r')
	# quality=fichier.readline().rstrip('\n\r')
	# duration=fichier.readline().rstrip('\n\r')
	# genre=fichier.readline().rstrip('\n\r')
	# director=fichier.readline().rstrip('\n\r')
	# year=fichier.readline().rstrip('\n\r')
	# synopsis=fichier.readline().rstrip('\n\r')
	
	# handle_click='plugin://plugin.video.zonedlclick'
	# url= handle_click + '?' + url_zonedl
	
	# li = xbmcgui.ListItem(title, iconImage=icon, thumbnailImage=icon)
	# li.setInfo('video', { 'year': year })
	# li.setInfo('video', { 'genre': genre })
	# li.setInfo('video', { 'duration': duration }) 
	# li.setInfo('video', { 'rating': note })
	# li.setInfo('video', { 'plot': synopsis })
	# li.setInfo('video', { 'director': director })
	# li.setLabel(title)
	# xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
	# fichier.close()
	
# xbmcplugin.endOfDirectory(addon_handle)












#### For folders ######
import os
import sys
import urllib
import urlparse
import xbmcgui
import xbmcplugin

base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
args = urlparse.parse_qs(sys.argv[2][1:])

xbmcplugin.setContent(addon_handle, 'movies')

def build_url(query):
    return base_url + '?' + urllib.urlencode(query)

mode = args.get('mode', None)

if mode is None:

	url = build_url({'mode': 'folder', 'foldername': 'Films'})
	li = xbmcgui.ListItem('Films', iconImage='https://image.tmdb.org/t/p/w185/iHyW1522lgxAY76jCqMD1dA60DK.jpg')
	li.setInfo('video', { 'plot': 'Rechercher des films sur le site Zone Téléchargement' })
	li.setInfo('video', { 'year': 'Pas encore fontionnel' })
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,listitem=li, isFolder=True)
	url = build_url({'mode': 'folder', 'foldername': 'Series'})
	li = xbmcgui.ListItem('Sécries', iconImage='https://image.tmdb.org/t/p/w185/wQoosZYg9FqPrmI4zeCLRdEbqAB.jpg')
	li.setInfo('video', { 'plot': 'Rechercher des séries sur le site Zone Téléchargement' })
	li.setInfo('video', { 'year': 'Pas encore fontionnel' })
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,listitem=li, isFolder=True)
	xbmcplugin.endOfDirectory(addon_handle)



elif mode[0] == 'folder':
	foldername = args['foldername'][0]
	### Ici c'est pour rechercher les films ###
	if foldername == 'Films':
		os.chdir("/storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films")
		dialog_search = xbmcgui.Dialog()
		search_name = dialog_search.input('Quel film voulez-vous rechercher ?', type=xbmcgui.INPUT_ALPHANUM)
		# print search_name
		file_title_search = open('/storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films/temp/file_title_search.txt','w')
		file_title_search.write(search_name)
		file_title_search.close()

		## Création progress bar #################
		progress = xbmcgui.DialogProgress()
		progress.create('Recherche du film', 'En cours...')
		######################################################################

		# Gestion de l'annulation de la recherche de qualité - au cas ou c'est trop long - arrêt du programme si "annulation" est cliqué #
		if progress.iscanceled() == 1:
			sys.exit(0)
		##################################################################################################################################

		
		line_to_bash='/storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films/search_scrap.sh'
		os.system(line_to_bash)
		progress.close()
		os.chdir("/storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films/data")

		for fich in os.listdir('.'):
			fichier = open(fich, "r")		
			title=fichier.readline().rstrip('\n\r')
			url_zonedl=fichier.readline().rstrip('\n\r')
			icon=fichier.readline().rstrip('\n\r')
			note=fichier.readline().rstrip('\n\r')
			quality=fichier.readline().rstrip('\n\r')
			duration=fichier.readline().rstrip('\n\r')
			genre=fichier.readline().rstrip('\n\r')
			director=fichier.readline().rstrip('\n\r')
			year=fichier.readline().rstrip('\n\r')
			synopsis=fichier.readline().rstrip('\n\r')
			id=fichier.readline().rstrip('\n\r')
			
			handle_click='plugin://plugin.video.zonedlclick'
			url= handle_click + '?' + url_zonedl + '?' + year	

			li = xbmcgui.ListItem(title, iconImage=icon, thumbnailImage=icon)
			li.setInfo('video', { 'year': year })
			li.setInfo('video', { 'genre': genre })
			li.setInfo('video', { 'duration': duration }) 
			li.setInfo('video', { 'rating': note })
			li.setInfo('video', { 'plot': synopsis })
			li.setInfo('video', { 'director': director })
			li.setLabel(title)
			

			ba_youtube = 'PlayMedia(plugin://plugin.video.youtube/?path=/root/video&action=play_video&videoid=' + id + ')'
			ba_youtube_for_movieinfo = 'plugin://plugin.video.youtube/?path=/root/video&action=play_video&videoid=' + id
			li.setInfo('video', {'trailer': ba_youtube_for_movieinfo })
			li.addContextMenuItems([("Bande annonce",ba_youtube),('Informations','XBMC.Action(Info)')], True)

			xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
			fichier.close()
		xbmcplugin.endOfDirectory(addon_handle)
	############################################
	
	### Ici pour les séries #####
	if foldername == 'Series':
		dialog = xbmcgui.Dialog()
		ok = dialog.ok('Pas encore supporté', 'La recherche des séries arrivera bientôt...' )
		sys.exit(0)
	#############################
    # xbmcplugin.endOfDirectory(addon_handle)


