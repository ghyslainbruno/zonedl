#!/bin/sh
cd /storage/.kodi/addons/plugin.video.zonedlclick

> http_codes.txt
for links in `cat not_checked_links.txt`
do 
	if [ `echo $links | grep 'uptobox' | wc -l` = "0" ]
	then
		curl -sL $links --write-out %{http_code} --output /dev/null >> http_codes.txt
		echo -e "\n" >> http_codes.txt
	else
		if [ `curl -sL $links | grep $links | wc -l` = "0" ]
		then
			echo '404' >> http_codes.txt
			echo -e "\n" >> http_codes.txt
		else
			echo '200' >> http_codes.txt
			echo -e "\n" >> http_codes.txt
		fi
	fi
done

bool_valid=`cat http_codes.txt | grep 404 | wc -l`


if [ "$bool_valid" = "0" ]
then
	echo All links valid
	echo "0" > nbr_links_invalid.txt
else 
	echo "1" > nbr_links_invalid.txt
	echo Some links invalid
fi