#!/bin/sh

old_IFS=$IFS  				
IFS=$'\n' 

cd /storage/.kodi/addons/plugin.video.zonedlclick

file='films'
title=`cat titre.txt | sed -e 's/À/A/g' | sed -e 's/ô/o/g' | sed -e 's/é/e/g' | sed -e 's/è/e/g' | sed -e 's/à/a/g' | sed -e 's/â/a/g' | sed -e 's/ê/e/g' | sed -e 's/î/i/g'`

> bool_dl_finished.txt
sidDS=`cat sid_download_station.txt | grep -o 'sid".*"}' | sed -e 's/sid\":\"//g' | sed -e 's/\"}//g'`
# curl -s "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=list&additional=detail,transfer&_sid=$sidDS" | sed -e 's/\"additional\":/\n/g' | sed -e 's/^.*"destination"/destination/g' | sed -e 's/"priority".*"size_downloaded"/"size_downloaded"/g' | sed -e 's/"size_uploaded".*"id"/"id"/g' | sed -e 's/,"status_extra".*$//g' | grep '^destination' > list_downloading.txt
# curl -s "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=list&additional=detail,transfer&_sid=$sidDS" | sed -e 's/\"additional\":/\n/g' | sed -e 's/^.*"destination"/destination/g' | sed -e 's/"priority".*"size_downloaded"/"size_downloaded"/g' | sed -e 's/"size_uploaded".*"id"/"id"/g' | sed -e 's/,"status_extra".*$//g' | grep '^destination' > list_downloading.txt
response=`curl -v --retry 10 "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=list&additional=detail,transfer&_sid=$sidDS"`
echo $response | sed -e 's/\"additional\":/\n/g' | sed -e 's/^.*"destination"/destination/g' | sed -e 's/"priority".*"size_downloaded"/"size_downloaded"/g' | sed -e 's/"size_uploaded".*"id"/"id"/g' | sed -e 's/,"status_extra".*$//g' | grep '^destination' > list_downloading.txt
# echo $response
# nbr_file_dl=`cat list_downloading.txt | grep '^destination' | sed -e 's/,\"size_downloaded.*$//g' | grep "$file" | sort | uniq | grep -o '\".*\"' | sed -e 's/"//g' | sed -e 's/films\///g' | grep $title | wc -l`
nbr_file_dl=`cat list_downloading.txt | grep "$title" | wc -l`


if [ $nbr_file_dl = "0" ]
then 
	echo $title is not downloading
	echo '2' > bool_dl_finished.txt
	echo '0' > percent_downloaded.txt
else
	if [ `cat list_downloading.txt | grep "$title" | grep '"status":"error"' | wc -l` != '0' ]
	then
		# Use this one when already seen an error...
		# error=`echo $response | sed -e 's/\"additional\":/\n/g' | sed -e 's/^.*"destination"/destination/g' | sed -e 's/"priority".*"size_downloaded"/"size_downloaded"/g' | sed -e 's/"size_uploaded".*"id"/"id"/g' | grep '^destination' | grep -o '"error_detail":".*"' | sed -e 's/\"},\"title\".*$//g' | sed -e 's/\"error_detail\":\"//g' | head -1`
		error='Something went wrong...'
		echo '3' > bool_dl_finished.txt
		echo $error > details_error_download.txt
		echo '0' > percent_downloaded.txt
		echo Errors occured
		for id in `cat list_downloading.txt | grep "$destinations" | grep -o 'dbid_[0-9][0-9][0-9][0-9]*'`
		do
			curl -s "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=delete&id=$id&_sid=$sidDS" 
			# > /dev/null 2>&1 &
		done
	else
		# for destinations in `cat list_downloading.txt | grep -m 1 '^destination' | sed -e 's/\",\"seedelapsed.*//g' | grep -o "$file.*"`
		# for destinations in `cat list_downloading.txt | grep '^destination' | sed -e 's/,\"size_downloaded.*$//g' | grep "$file" | sort | uniq | grep -o '\".*\"' | sed -e 's/"//g' | sed -e 's/films\///g' | grep $title`
		# do
		# 	nbr_links_total=`cat list_downloading.txt | grep "$destinations" | wc -l`
		# 	nbr_links_finished=`cat list_downloading.txt | grep "$destinations" | grep 'finished' | wc -l`
		# 	if [ "$nbr_links_total" = "$nbr_links_finished" ]
		# 	then
		# 		for id in `cat list_downloading.txt | grep "$destinations" | grep -o 'dbid_[0-9][0-9][0-9][0-9]*'`
		# 		do
		# 			# echo On supprime ce DL $id
		# 			curl -s "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=delete&id=$id&_sid=$sidDS" 
		# 			# > /dev/null 2>&1 &
		# 		done
		# 		# destination_to_disp=`echo $destinations | sed -e "s/$file\///g"`
		# 		# date
		# 		echo $destinations is done
		# 		curl -s --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Scan", "id": "mybash"}' -H 'content-type: application/json;' http://192.168.1.72:8080/jsonrpc 
		# 		echo '1' > bool_dl_finished.txt
		# 		echo '0' > percent_downloaded.txt
		# 		# echo $nbr_links_total >> bool_dl_finished.txt
		# 		# echo $nbr_links_finished >> bool_dl_finished.txt
		# 		# > /dev/null 2>&1 &
		# 	else
		# 		echo All DL are not done  
		# 		if [ `cat list_downloading.txt | grep -o '"size":"[0-9]"' | sed -e 's/\"size\":\"//g' | sed -e 's/\"//g' | uniq | grep '^0$' | wc -l` = '0' ]
		# 		then
		# 			cat list_downloading.txt | grep $destinations | sed -e 's/destination.*\"size_downloaded\"/\"size_downloaded\"/g' | sed -e 's/,\"id\".*\"size\"/\"size\"/g' | sed -e 's/,\"status\".*$//g' | sed -e 's/\"\"/\"\ \"/g' | sed -e 's/\"size_downloaded\":\"//g' | sed -e 's/\"\ \"size\":\"/\ /g' | sed -e 's/\"//g' | awk '{SUM1 += $1, SUM2 += $2, TOTAL = (SUM1/SUM2)*100} END {print int(TOTAL)}' > percent_downloaded.txt
		# 		else 
		# 			echo '0' > percent_downloaded.txt
		# 		fi
		# 		echo '0' > bool_dl_finished.txt
		# 		# > /dev/null 2>&1
		# 	fi
		# done

		# for destinations in `cat list_downloading.txt | grep  "$title"`
		# do


			# currentMovieDownloadingInformation=`cat list_downloading.txt | grep "$title"`
			nbr_links_total=`cat list_downloading.txt | grep "$title" | wc -l`
			nbr_links_finished=`cat list_downloading.txt | grep "$title" | grep 'finished' | wc -l`
			totalSize=`cat list_downloading.txt | grep "$title" | grep -o '\"size\":[0-9]*' | grep -o '[0-9]*' | awk '{SUM += $1} END {print SUM}'`

			# nbr_links_total=`cat list_downloading.txt | grep "$destinations" | wc -l`
			# nbr_links_finished=`cat list_downloading.txt | grep "$destinations" | grep 'finished' | wc -l`
			if [ "$nbr_links_total" = "$nbr_links_finished" ]
			then
				for id in `cat list_downloading.txt | grep "$title" | grep -o 'dbid_[0-9][0-9][0-9][0-9]*'`
				do
					echo On supprime ce DL $id
					curl -s "http://192.168.1.71:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=delete&id=$id&_sid=$sidDS" 
					# > /dev/null 2>&1 &
				done
				# destination_to_disp=`echo $destinations | sed -e "s/$file\///g"`
				# date
				echo $title is done
				curl -s --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Scan", "id": "mybash"}' -H 'content-type: application/json;' http://192.168.1.72:8080/jsonrpc 
				echo '1' > bool_dl_finished.txt
				echo '0' > percent_downloaded.txt
				# echo $nbr_links_total >> bool_dl_finished.txt
				# echo $nbr_links_finished >> bool_dl_finished.txt
				# > /dev/null 2>&1 &
			else
				echo All DL are not done  
				if [ `echo $totalSize | grep '^0$' | wc -l` = '0' ]
				then
					cat list_downloading.txt | grep "$title" | grep -o '\"size_downloaded\":[0-9]*' | grep -o '[0-9]*' | awk '{SUM += $1, TOTAL = (SUM/'"$totalSize"')*100}END {print int(TOTAL)}' > percent_downloaded.txt
				else 
					echo '0' > percent_downloaded.txt
				fi
				echo '0' > bool_dl_finished.txt
				# > /dev/null 2>&1
			fi
		# done
	fi
fi

# On se deloggue #
# curl -so "http://192.168.1.71:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=1&method=logout&session=DownloadStation"  > /dev/null 2>&1
##################