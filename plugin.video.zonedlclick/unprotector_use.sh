#!/bin/bash
cd /storage/.kodi/addons/plugin.video.zonedlclick

old_IFS=$IFS  				
IFS=$'\n' 

> not_checked_links_json.txt
for dl_protect_link in `cat heberg_premium.txt`
do 
	id=`echo $dl_protect_link | grep -o '[A0-Z9][A0-Z9][A0-Z9][A0-Z9][A0-Z9][A0-Z9][A0-Z9][A0-Z9]'`
	curl -s "http://unprotector.jimrobs.com/solve?id=$id" >> not_checked_links_json.txt
	echo link unprotected
done

cat not_checked_links_json.txt | sed -e 's/}/\n/g' | grep -Eo '\"http://(ul.to/|uploaded.net/|uptobox.com/).*\"' | sed -e 's/\"//g' | sed -e 's/,/\n/g' > not_checked_links.txt



### Test validation liens ###
	# rm temp_html/*.html
	# num_link=1
	# echo Validation links in progress...
	# for links in `cat not_checked_links.txt`
	# do
		## rajout du test pour les redirections des liens ul.to
		# bool_ul=`echo $links | grep 'ul.to' | wc -l`
		# if [ "$bool_ul" = "1" ]
		# then
			# curl -Ls $links > temp_html/$num_link.html	
			# num_link=$(( $num_link + 1 ))			
		# else
			# curl -s $links > temp_html/$num_link.html		
			# num_link=$(( $num_link + 1 ))			
		# fi
	# done
	
	# > nbr_links_invalid.txt
	# >temp_html/nbrLinesPerFile.txt
	# for file in `ls temp_html/*.html`
	# do 
		# cat $file | wc -l >> temp_html/nbrLinesPerFile.txt
	# done
	# nbr_links_invalid=`cat temp_html/nbrLinesPerFile.txt | grep '^0$' | wc -l`
	# echo $nbr_links_invalid >> nbr_links_invalid.txt
	
	# if [ "$nbr_links_invalid" = "0" ]
	# then 
		# echo All links are valid 
	# else
		# echo Some links invalid
	# fi	

#############################


####### Autre façon (plus "robuste") de checker la validité des liens ########
> http_codes.txt
for links in `cat not_checked_links.txt`
do 
	curl -sL $links --write-out %{http_code} --output /dev/null >> http_codes.txt
	echo -e "\n" >> http_codes.txt
done

bool_valid=`cat http_codes.txt | grep 404 | wc -l`


if [ "$bool_valid" = "0" ]
then
	echo All links valid
	echo "0" > nbr_links_invalid.txt
else 
	echo "1" > nbr_links_invalid.txt
	echo Some links invalid
fi

################################################################################



IFS=$old_IFS