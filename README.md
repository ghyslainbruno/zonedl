# ZoneDL

## Project description
This project downloads Movies from Zone telechargement website directly into user's NAS Synology storage (kind of ancestor of Lazyker project). 
It is a Kodi plugin, made in Python. 

## When ZoneDL came in my developer journey - Oct 2016
It was actually my 2nd development project I've ever done in my whole life. At this time, I didn't follow any IT course / lecture. 

So I was basically experiencing something that I loved : developing.    

## What I learned with ZoneDL
Pretty much the same things that I learned with "Shows" project, but I learned far more Python with this one. 

### ZoneDL : Not hosted anywhere - could be a good project to start this one from scratch again (the project)
 
# Cheat sheet for personnal use 

## ! CAREFUL ! Unusable, since the website used to populate this plugin is down ! 

## README of the KODI addon for zone-telechargement website


This project is still in progress (will use ZoneDlBackEnd DB soon) and will be translated in a full python project sometime. 
but for now it is a mix between python and shell codes.

For now, only "Uploaded" and "Uptobox" hosts are supported. 

---

To use this pluggin you must have the following setup : 
- Use Kodi on Openelec/LibreElec (may work on other ones, but not tested)
- Having a Synology NAS with DownloadStation installed on it
- Knowing ip and credentials of NAS and Kodi device

---

Here's what the pluggin can do :
 - Possibility to search any film on the website (independant of the website soon cf zoneDlBackEnd project)
 - Browse the "Exclus" movies
 - Can launch youtube trailer before download (need Youtube plugin installed on Kodi)
 - And Download each one of them (need to some user informations in DownloadStation about hosts)
  
 *Comment : The pluggin uses 2Captcha services to bypass dl-protect captcha when it's needed*

---

# How to : install 
- Clone/download the project
- ch in the download directory


---

## Copy all files to the device used
``` shell
scp -r * root@ip_of_device:/storage/.kodi/addons
```

---

# How to : Create a cron Job for daily scrapping 
For starting scrapping daily at 00:00
``` shell 
echo "0 0 * * * /storage/.kodi/addons/plugin.video.zonedl/ressources/scrap_exclus_movies.sh | tee -a  /storage/.kodi/addons/plugin.video.zonedl/ressources/fich.log" | crontab - 
```

Here is a video demo : 


[![Video demo](https://gitlab.com/Ghyslain/zonedl/raw/bc763bce1318cbad5e55f5fdcf7bdb9cfc75263b/demo_video_image_zonedl.png)](https://youtu.be/DT1cyPrRW2U)
